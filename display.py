def add_latlng(recommend_spots)->list: #recommend_spotsの緯度経度を追加
    places={}
    #places=[]#現在のスポットから一番近い場所を探すのに必要
    for n, address in enumerate(recommend_spots['住所']):
        try:#住所の前処理が必要な物は飛ばしている
            latlon = geocoder.arcgis(address)
            recommend_spots.loc[n,'緯度'] = latlon.json["lat"]
            recommend_spots.loc[n,'経度'] = latlon.json["lng"]
            #recommend_spots.loc[n,'index'] = recos.index[n]

            place=[latlon.json["lat"],latlon.json["lng"]]
            places.update({recommend_spots.index[n]:place})
        except:
            continue
        #index_place=dict(zip(recommend_spots))
    return places

def rootbylatlng4root(current:list, places:dict, recommend_spots):
    root_index=[]
    root=[current]
    spot2spot=[]
    while places:
        minlong=100
        nearlest=0
        for i in places:
            dis=geodesic(root[-1], places[i]).km
            if dis < minlong:
                minlong=dis
                nearlest=places[i]
                index=i
        spot2spot.append(minlong)
        root.append(nearlest)
        root_index.append(index)
        del places[index]
    recommend_spots=recommend_spots.iloc[root_index]
    recommend_spots2=recommend_spots.copy()
    recommend_spots2['距離']=spot2spot
    recommend_spots2['時間(分、自転車)']=recommend_spots2['距離']/15*60
    recommend_spots2['時間(分、徒歩)']=recommend_spots2['距離']/4*60
    return root, recommend_spots2

def sort_root(current:list, recommend_spots)->list:
    """
    現在地とおすすめスポットの緯度経度を引数に
    現在のスポットから近い順に並べ替える
    サブモジュールadd_latlng, rootbylatlngが必要
    """
    recommend_spots.reset_index(drop=True, inplace=True)

    places=add_latlng(recommend_spots)#現在のスポットから一番近い場所を探すのに必要
    
    root, recommend_spots=rootbylatlng4root(current, places, recommend_spots)#現在のスポットから一番近い場所をrootに追加していく

    return root, recommend_spots

def display_recommend_root(current:list, root:list, recommend_spots)->map:
    map = folium.Map(location=current, zoom_start=12)
    folium.PolyLine(locations=root).add_to(map)
    for n,(lat, lng) in enumerate(zip(recommend_spots['緯度'],recommend_spots['経度'])):
        try:#緯度経度が無い物は飛ばしている。sort_root関数で前処理が必要
            html=f"<a href ={recommend_spots['Instagram'].iloc[n]} target='_blank' rel='noopener noreferrer'>{recommend_spots['店舗名'].iloc[n]}</a>"
            iframe = branca.element.IFrame(html=html, width=300, height=100)
            popup = folium.Popup(iframe, max_width=300)
            folium.Marker(location=[lat, lng], popup=popup).add_to(map)
        except:
            continue
        
    map.save("map_train_test.html")
    return map
