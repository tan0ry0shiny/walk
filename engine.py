def usable(spot_info, use_date, stay_time) -> str:
    """
    spot_info:df, use_date:df, stay_time:datetime
    体験可能かどうかを返します。
    1. 店舗情報の取得,2. 利用期間の入力,3. 利用可能か判定
    の手順で判定します。
    以下のサブモジュールが必要です。
    shop_info, change_time, able_judge, timable
    """
    #1. 店舗情報の取得
    #run_times, holiday=shop_info(xml_text)#XMLから営業時刻を抽出
    run_times_sr=spot_info['営業時間']#XMLから営業時刻を抽出
    #2. 利用期間の取得
        #2-1. 利用時間を取得
    ex_time0 = change_time(use_date.split()[1])#滞在開始時刻
    ex_time1 = ex_time0 + change_time(stay_time)#滞在終了時間
    ex_times=[ex_time0, ex_time1]
        #2-2. 曜日を抽出
    # week_list = ["月", "火", "水", "木", "金", "土", "日"]
    # week_index=datetime.strptime(use_date, "%Y/%m/%d %H:%M:%S")
    # week = week_list[week_index.weekday()]
    #3. 利用可否判定
        #3-1. どの営業時間帯を使うか判別する
    run_times_box = change_time_format(run_times_sr)
    #print(run_times_box)
    anses=[]
    for run_times in run_times_box:
        ans = able_judge(run_times, ex_times)#, week, holiday)
        anses.append(ans)
    return anses

#フォーマットに沿って表現を変更する関数
def change_time(time):
    """
    時間の計算をプログラミングで計算しやすい様に10進数に直します
    """
    abstime=float(time.split(':')[0])+float(time.split(':')[1])/60
    return abstime

def timable(run_time, ex_time):
    """
    お店の営業時間と利用する時間から、利用できるかどうかを返します
    """
    if run_time[0] < ex_time < run_time[1]:
        ans = "利用できます"
    else:
        ans = "利用できません"
    return ans

def change_time_format(time_sr):
    run_times_box=[]
    for n, oh in enumerate(time_sr):
        try:
            res = re.findall(r'[0-9]+:[0-9]+', mojimoji.zen_to_han(oh))
            run_times_box.append(res)
        except:
            run_times_box.append('登録されていません')
    return run_times_box

def able_judge(run_times, ex_times) -> str:#, week, holiday):
    times=[]
    if len(run_times)==2:
        run_time=[change_time(run_times[0]),change_time(run_times[1])]
        times.append(run_time)
    else:
        for time in times:
            run_time=[change_time(time[0]),change_time(time[1])]
            times.append(run_time)
    anses=[]
    for run_time in times:
        if timable(run_time, ex_times[0]) == timable(run_time, ex_times[1]) =="利用できます":#and not(week in holiday):
            temp_ans = "利用可能"
        else:
            temp_ans = "利用不可能"
        anses.append(temp_ans)
    ans=""
    for temp_ans in anses:
        if temp_ans == "利用可能":
            ans = "利用可能"
    if ans is "":
        ans = "利用不可能"
    return ans

def satable(input:str, df, mask:list, rank_n:int):
    """
    transformerを使って、入力文と店舗概要の類似度を出します。
    df['店舗名'].iloc[int(idx)]が1つずれている可能性がある。0オリジンかの問題
    """
    corpus = df['概要'].values[mask]
    corpus_embeddings = model.encode(corpus, convert_to_tensor=True)
    query_embedding = model.encode(input, convert_to_tensor=True)
    cos_scores = util.cos_sim(query_embedding, corpus_embeddings)[0]
    try:
        top_results = torch.topk(cos_scores, k=rank_n)
    except:
        return "利用できる店舗が少なくなっています。時間帯や店舗数を変更して下さい。"
    #reco_dict={"スポット名":df['スポット名'].iloc[int(idx)],"スコア":f"score:{score:.4f}","概要":df['概要'].iloc[int(idx)],"住所":df['住所'].iloc[int(idx)]] for idx, score in zip(top_results[1],top_results[0])}
    reco_df=df.iloc[top_results[1]]
    reco_df2=reco_df.copy()
    reco_df2.reset_index(drop=True ,inplace=True)
    reco_df2['score']=top_results[0]
    # print(f"希望に近いお店TOP{rank_n}")
    # try:
    #     for n, reco_dic in enumerate(reco_df2['店舗名']):
    #         print(f"{n+1}:{reco_dic}{reco_df2[reco_df2['店舗名']==reco_dic]['概要']}")
    # except:
    #     return print(reco_dic)
    return reco_df2

def exable(df, use_date, stay_time, input:str, rank_n:int):
    usables=usable(df, use_date, stay_time)
    usables_list=[i=="利用可能" for i in usables]
    print(f"希望に近い店舗Top {rank_n} :")
    reco_df=satable(input=input, df=df, mask=usables_list,rank_n=rank_n)
    display(reco_df)
#try:
        #for n, content in enumerate(reco_df):
            #display(reco_df.iloc[n])#(f"{content}{reco_dict[content]}")
   # except:
        #return print(reco_dict)
    return reco_df

