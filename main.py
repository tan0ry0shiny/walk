import pandas as pd
import requests
from bs4 import BeautifulSoup
from fastprogress.fastprogress import  progress_bar
import pandas as pd
import mojimoji
from datetime import datetime, timedelta
import re
import numpy as np
from sentence_transformers import SentenceTransformer, util
import torch
#for display
import geocoder
import folium
import pandas as pd
import branca
from geopy.distance import geodesic
from data_collect import ChoshiKanko_Scraping
from engine import *
from display import *


df = ChoshiKanko_Scraping("https://www.choshikanko.com/kankoDB/", test_n=False)

model = SentenceTransformer('paraphrase-multilingual-mpnet-base-v2')

# use_date="2022/08/2 15:30"
# stay_time="0:30"
# input="景色を楽しみながら海の幸を満喫したい"
recos=exable(df, use_date, stay_time, input, rank_n=10)

current=[35.734645390000026,140.8266930200001]#銚子駅

root, recommend_spots = sort_root(current, recos)#recosにもlat,lngが追加される。

recommend_spots['時間(分)']=recommend_spots['距離']/15*60

display_recommend_root(current, root, recommend_spots)
