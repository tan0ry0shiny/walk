def ChoshiKanko_Scraping(base_url:str, test_n=5):
    """
    銚子観光公式HP用のスクレイピングの関数です。
    """

    #base_url="https://www.choshikanko.com/kankoDB/"
    response = requests.get(base_url)
    soup = BeautifulSoup(response.text, 'html.parser')

    page_links=[]
    for i in range(6):
        link=f"{base_url}page/{i+1}/"
        page_links.append(link)
    #print("各店舗のリンクを取得中")
    store_urls=[]
    for page_link in page_links:
        response = requests.get(page_link)
        soup = BeautifulSoup(response.text, 'html.parser')
        store_links=soup.find_all('a', class_='link-item')
        for store_link in store_links:
            store_urls.append(store_link.get('href'))

    df = pd.DataFrame()
    error_urls=[]
    #print("店舗情報の取得開始")
    if test_n:
        print("test mode")
        for n, url in enumerate(progress_bar(store_urls[:test_n])):
            if (n+1)<=test_n:
                dfa=pd.read_html(url, header=None)
                dfa=dfa[0].set_index(0).T
                shop_name, abst_phrase = shop_name_abst(url)
                dfa["店舗名"]=shop_name
                dfa["概要"]=abst_phrase
                df=df.append(dfa)
    else:#全ページのスクレイピング
        for n, url in enumerate(progress_bar(store_urls)):
            if (n+1)%30==0:
                continue#print(f"{n+1}件目の情報を取得")
            try:
                dfa=pd.read_html(url, header=None)
                dfa=dfa[0].set_index(0).T
                shop_name, abst_phrase = shop_name_abst(url)
                dfa["店舗名"]=shop_name
                dfa["概要"]=abst_phrase
                df=df.append(dfa)
            except:
                error_urls.append(url)
            time.sleep(1)
        #print(f"{n+1}件目の情報を取得")
    return df

def shop_name_abst(url:str) -> str:
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    shop_name = soup.find('div', class_='db_title')
    shop_abst=soup.find('div', class_='text-block_typeA')
    rejects=["\n","\r"," "]
    abst_phrase=shop_abst.text
    shop_name=shop_name.text
    for text in rejects:
        abst_phrase=abst_phrase.replace(text, '')
        shop_name=shop_name.replace(text, '')
    abst_phrase=mojimoji.zen_to_han(abst_phrase)
    shop_name=mojimoji.zen_to_han(shop_name)
    return shop_name, abst_phrase
